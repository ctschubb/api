﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using sample.Authorization.Users;
using sample.MultiTenancy;
using sample.Projects;
using System;
using System.Collections.Generic;
using System.Text;

namespace sample.Tasks
{
    public class Task: FullAuditedEntity<long>, IMustHaveTenant
    {
        public virtual string TaskName { get; set; }

        public virtual string Description { get; set; }

        public virtual long UserId { get; set; }

        public virtual long ProjectId { get; set; }

        public virtual int TenantId { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual Project Project { get; set; }

        public virtual Tenant Tenant { get; set; }

        public virtual User User { get; set; }
    }
}
