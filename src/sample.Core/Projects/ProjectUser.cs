﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace sample.Projects
{
    public class ProjectUser :Entity<long>
    {
        public long ProjectId { get; set; }

        public long UserId { get; set; }
    }
}
