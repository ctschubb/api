﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using sample.MultiTenancy;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace sample.Projects
{
    public class Project : FullAuditedEntity<long>, IMustHaveTenant
    {
        public virtual string Name { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual int TenantId { get; set; }

        public Tenant Tenant { get; set; }
    }
}
