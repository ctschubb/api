﻿namespace sample.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";

        public const string Pages_Roles = "Pages.Roles";

        public const string Pages_Tasks = "Pages.Tasks";

        public const string Pages_Projects = "Pages.Projects";

        public const string Pages_Project_Users = "Pages.Project_Users";

        public const string Pages_Project_Host = "Pages.Project_Host";

        public const string Pages_Task_Host = "Pages.Task_Host";
    }
}
