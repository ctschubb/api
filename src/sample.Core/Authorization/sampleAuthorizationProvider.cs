﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace sample.Authorization
{
    public class sampleAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            context.CreatePermission(PermissionNames.Pages_Tasks, L("Tasks"), multiTenancySides: MultiTenancySides.Tenant);
            context.CreatePermission(PermissionNames.Pages_Projects, L("Projects"), multiTenancySides: MultiTenancySides.Tenant);
            context.CreatePermission(PermissionNames.Pages_Project_Users, L("ProjectUsers"));
            context.CreatePermission(PermissionNames.Pages_Project_Host, L("ProjectHost"), multiTenancySides:MultiTenancySides.Host);
            context.CreatePermission(PermissionNames.Pages_Task_Host, L("TaskHost"), multiTenancySides:MultiTenancySides.Host);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, sampleConsts.LocalizationSourceName);
        }
    }
}
