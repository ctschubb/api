using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace sample.EntityFrameworkCore
{
    public static class sampleDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<sampleDbContext> builder, string connectionString)
        {
            builder.UseNpgsql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<sampleDbContext> builder, DbConnection connection)
        {
            builder.UseNpgsql(connection);
        }
    }
}
