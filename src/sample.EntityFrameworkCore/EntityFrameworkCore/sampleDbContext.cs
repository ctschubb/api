﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using sample.Authorization.Roles;
using sample.Authorization.Users;
using sample.MultiTenancy;
using sample.Tasks;
using sample.Projects;

namespace sample.EntityFrameworkCore
{
    public class sampleDbContext : AbpZeroDbContext<Tenant, Role, User, sampleDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<ProjectUser> ProjectUsers { get; set; }

        public sampleDbContext(DbContextOptions<sampleDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Abp.Localization.ApplicationLanguageText>()
                .Property(p => p.Value)
                .HasMaxLength(100); // any integer that is smaller than 10485760
        }
    }
}
