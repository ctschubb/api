﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using Abp.MultiTenancy;
using Abp.Runtime.Security;
using sample.Authorization;
using sample.Authorization.Roles;
using sample.Authorization.Users;
using sample.Editions;
using sample.MultiTenancy.Dto;
using Microsoft.AspNetCore.Identity;
using sample.Tasks.Dto;
using Abp.Runtime.Session;

namespace sample.Tasks
{
    [AbpAuthorize(PermissionNames.Pages_Tasks)]
    public class TaskAppService : AsyncCrudAppService<Task, TaskDto, long, PagedTaskRequestDto, CreateTaskDto, TaskDto>, ITaskAppService
    {
        private readonly IRepository<Task, long> _taskRepository;
        private readonly IAbpSession _abpSession;

        public TaskAppService(IRepository<Task, long> taskRepository, IAbpSession abpSession) : base(taskRepository)
        {
            _taskRepository = taskRepository;
            _abpSession = abpSession;
        }

        public override Task<TaskDto> CreateAsync(CreateTaskDto input)
        {
            input.UserId = _abpSession.GetUserId();
            return base.CreateAsync(input);
        }

        public override Task<TaskDto> UpdateAsync(TaskDto input)
        {
            input.UserId = _abpSession.GetUserId();
            return base.UpdateAsync(input);
        }
    }
}

