﻿using Abp.Application.Services;
using sample.MultiTenancy.Dto;
using sample.Tasks.Dto;

namespace sample.Tasks
{
    public interface ITaskAppService : IAsyncCrudAppService<TaskDto, long, PagedTaskRequestDto, CreateTaskDto, TaskDto>
    {
    }
}

