﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Microsoft.EntityFrameworkCore;
using sample.Authorization;
using sample.Projects;
using sample.Tasks.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sample.Tasks
{
    [AbpAuthorize(PermissionNames.Pages_Task_Host)]
    public class TaskHostAppservice : ITaskHostAppservice
    {
        private readonly IRepository<Task, long> _repository;
        private readonly IRepository<ProjectUser, long> _projectUsersRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IAbpSession _abpSession;

        public TaskHostAppservice(IRepository<Task, long> repository, IRepository<ProjectUser, long> projectUsersRepository, IUnitOfWorkManager unitOfWorkManager, IAbpSession abpSession)
        {
            _repository = repository;
            _projectUsersRepository = projectUsersRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _abpSession = abpSession;
        }

        public List<TaskDto> GetAllTasks()
        {
            var userId = _abpSession.GetUserId();
            var projectIds = _projectUsersRepository.GetAll().Where(x => x.UserId == userId).Select(x=>x.ProjectId).ToList();
            var items = new List<TaskDto>();
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                items = _repository.GetAllList().Where(x=>projectIds.Contains(x.ProjectId)).Select(x => new TaskDto()
                {
                    Id = x.Id,
                    TaskName = x.TaskName,
                    IsActive = x.IsActive,
                    Description = x.Description,
                    UserName = x.User?.FullName,
                    TenantName = x.Tenant?.TenancyName,
                    ProjectName = x.Project?.Name
                }).ToList();
            };

            return items;
        }
    }
}
