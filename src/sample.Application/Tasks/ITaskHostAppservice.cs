﻿using Abp.Application.Services;
using sample.Tasks.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace sample.Tasks
{
    public interface ITaskHostAppservice: IApplicationService
    {
        List<TaskDto> GetAllTasks();
}