using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.MultiTenancy;
using sample.Tasks;

namespace sample.Tasks.Dto
{
    [AutoMapFrom(typeof(Task))]
    public class TaskDto : EntityDto<long>
    {
        [Required]
        [StringLength(AbpTenantBase.MaxNameLength)]
        public string TaskName { get; set; }

        [Required]
        public string Description { get; set; }

        public string ProjectName { get; set; }

        public string TenantName { get; set; }

        public string UserName { get; set; }

        public long UserId { get; set; }

        public bool IsActive {get; set;}
    }
}
