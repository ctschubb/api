﻿using Abp.Application.Services.Dto;

namespace sample.Tasks.Dto
{
    public class PagedTaskRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

