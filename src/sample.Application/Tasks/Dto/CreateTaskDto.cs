using System.ComponentModel.DataAnnotations;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.MultiTenancy;
using sample.Tasks;

namespace sample.Tasks.Dto
{
    [AutoMapTo(typeof(Task))]
    public class CreateTaskDto
    {
        [Required]
        [StringLength(AbpTenantBase.MaxNameLength)]
        public string TaskName { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public long ProjectId { get; set; }

        public long UserId { get; set; }
    }
}
