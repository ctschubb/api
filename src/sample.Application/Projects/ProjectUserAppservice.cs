﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using sample.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sample.Projects
{
    [AbpAuthorize(PermissionNames.Pages_Project_Users)]
    public class ProjectUserAppservice : IProjectUserAppService
    {
        private readonly IRepository<ProjectUser, long> _repository;

        public ProjectUserAppservice(
            IRepository<ProjectUser, long> repository)
        {
            _repository = repository;
        }

        public List<long> GetProjectsIdsByUserId (long userId)
        {
            return _repository.GetAll()
                .Where(x => x.UserId == userId)
                .Select(y => y.ProjectId)
                .ToList();
        }

        public void UpdateProjectIdsForUser (long userId, List<long> projectIds)
        {
            _repository.Delete(x => x.UserId == userId && !projectIds.Contains(x.ProjectId));

            foreach(var projectId in projectIds)
            {
                _repository.InsertOrUpdate(new ProjectUser { ProjectId = projectId, UserId = userId });
            }
        }
    }
}
