﻿using Abp.Application.Services.Dto;

namespace sample.Projects.Dto
{
    public class PagedProjectResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

