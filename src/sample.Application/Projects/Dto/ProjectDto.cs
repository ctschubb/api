using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.MultiTenancy;
using sample.Projects;

namespace sample.Projects.Dto
{
    [AutoMapFrom(typeof(Project))]
    public class ProjectDto : FullAuditedEntityDto<long>
    {
        [Required]
        [StringLength(AbpTenantBase.MaxNameLength)]
        public string Name { get; set; }

        public int TenantId { get; set; }

        public string TenantName { get; set; }

        public bool IsActive { get; set; }
    }
}
