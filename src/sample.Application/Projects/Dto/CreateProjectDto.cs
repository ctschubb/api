using System.ComponentModel.DataAnnotations;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.MultiTenancy;
using sample.Projects;

namespace sample.Projects.Dto
{
    [AutoMapTo(typeof(Project))]
    public class CreateProjectDto
    {
        [Required]
        [StringLength(AbpTenantBase.MaxNameLength)]
        public string Name { get; set; }
    }
}
