﻿using Abp.Application.Services;
using sample.MultiTenancy.Dto;
using sample.Projects.Dto;

namespace sample.Projects
{
    public interface IProjectAppService : IAsyncCrudAppService<ProjectDto, long, PagedProjectResultRequestDto, CreateProjectDto, ProjectDto>
    {
    }
}

