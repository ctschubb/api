﻿using System.Linq;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using Abp.MultiTenancy;
using Abp.Runtime.Security;
using sample.Authorization;
using sample.Authorization.Roles;
using sample.Authorization.Users;
using sample.Editions;
using sample.MultiTenancy.Dto;
using Microsoft.AspNetCore.Identity;
using sample.Projects.Dto;

namespace sample.Projects
{
    [AbpAuthorize(PermissionNames.Pages_Projects)]
    public class ProjectAppService : AsyncCrudAppService<Project, ProjectDto, long, PagedProjectResultRequestDto, CreateProjectDto, ProjectDto>, IProjectAppService
    {
        //private readonly ProjectManager _projectManager;
        private readonly IRepository<Project, long> _projectRepository;

        public ProjectAppService(
            //ProjectManager projectManager,
            IRepository<Project,long> projectRepository) : base(projectRepository)
        {
            //_projectManager = projectManager;
            _projectRepository = projectRepository;
        }
        
    }
}

