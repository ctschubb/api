﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using sample.Authorization;
using sample.Projects.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sample.Projects
{
    [AbpAuthorize(PermissionNames.Pages_Project_Host)]
    public class ProjectHostAppservice : IProjectHostAppservice
    {
        private readonly IRepository<Project, long> _repository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public ProjectHostAppservice(IRepository<Project, long> repository, IUnitOfWorkManager unitOfWorkManager)
        {
            _repository = repository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public List<ProjectDto> GetAllProjects()
        {
            using (var uow = _unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MustHaveTenant))
            {
                return _repository.GetAllIncluding(x => x.Tenant).Select(x => new ProjectDto()
                {
                    Id = x.Id,
                    Name = x.Name,
                    TenantId = x.TenantId,
                    TenantName = x.Tenant.Name
                }).ToList();
            }
        }
    }
}
