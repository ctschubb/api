﻿using Abp.Application.Services;
using sample.Projects.Dto;
using System.Collections.Generic;

namespace sample.Projects
{
    public interface IProjectHostAppservice: IApplicationService
    {
        List<ProjectDto> GetAllProjects();
    }
}