﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace sample.Projects
{
    public interface IProjectUserAppService : IApplicationService
    {
        List<long> GetProjectsIdsByUserId(long userId);

        void UpdateProjectIdsForUser(long userId, List<long> projectIds);
    }
}
